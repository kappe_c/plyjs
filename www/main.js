/**
 * This file is automatically loaded asynchronously by RequireJS because it is set as the value of 
 * the `data-main` attribute. 
 * The code here is responsible to set the configuration of RequireJS and to load the application 
 * by means of filling the HTML skeleton that is defined in the body of index.html.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 10, 2017
 */
require(['Testing'], (Testing) => 
{
	const fileInp = document.getElementById('fileInput');
	const copyInp = document.getElementById('copyInput');
	const formatSel = document.getElementById('formatSelect');
	const okBtn = document.getElementById('okButton');
	
	okBtn.onclick = () => {
		const file = fileInp.files[0];
		if(!file)
			return;
		Testing.read(file, (arrBuf, src) =>
		{
			Testing.test(arrBuf, src, copyInp.checked && formatSel.value);
		});
	};
} );
