define(() => 
/**
 * This class is not actually used by PlyJS itself but provided for users.
 * Instances of this class could be thrown after a call like `elems.getElement('elemName')`
 * when a software requires a certain element to be present in a PLY file.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date June 5, 2018
 */
class PlyMissingElementError extends Error {

/**
 * @param {string} eName Name of the required PLY element.
 * @param {string} [fName] Name of the source file (as from `elems.source`).
 */
constructor(eName, fName)
{
  super();
  this.message = 'A "' + eName + '" element is required';
  if(fName !== undefined)
    this.message += ' in "' + fName + '"';
}

} );
