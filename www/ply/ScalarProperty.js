define(['ply/Property'], (PlyProperty) => 
/**
 * Class representing a PLY property that stores scalars.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 11, 2017
 */
class PlyScalarProperty extends PlyProperty {

/** Verbose class name. */
static get label() { return 'PLY Scalar Property'; }

//------------------------------------------------------------------------------------------------//

/** Construct a `PlyScalarProperty` from a typed array. */
static fromData(name, data, comments)
{
	const type = data.constructor.name.match(/(\D+\d+)\D+/)[1];
	const obj = new PlyScalarProperty(type, name, 0, comments);
	obj._data = data;
// 	obj.compExtrema(); // NOTE May or may not be desired?
	return obj;
}

//------------------------------------------------------------------------------------------------//

/**
 * Constructor defined for the use in `PlyLoader.load`. Any further arguments are added to the 
 * data array (for convenient construction of properties with few values).
 * @param {string} type - Type of the items in `this.data`. 
 * One of the values in `PlyLoader.plyType2jsType`. 
 * @param {string} name - Name of this PLY property. 
 * @param {number} numItems - Number of items held by this PLY property, must be consistent with 
 * the number of objects in the parent PLY element.
 * @param {Array} [comments=[]] - Comments associated with this PLY property.
 */
constructor(type, name, numItems, comments = [])
{
	super(name, comments);
	this._type = type;
	this._data = new window[type + 'Array'](numItems);
	for(let i = 4; i < arguments.length; ++i)
		this._data[i - 4] = arguments[i];
}

//------------------------------------------------------------------------------------------------//

/** Get the (JS) type of the items. */
get type() { return this._type; }

//------------------------------------------------------------------------------------------------//

/** Impl. of `PlyProperty` method. */
get hasFloats() { return this._type === 'Float64' || this._type === 'Float32'; }

//------------------------------------------------------------------------------------------------//

/** Impl. of `PlyProperty` method. */
compExtrema()
{
	const data = this._data;
	const n = data.length;
	let minIdx, minVal, maxIdx, maxVal;
	maxIdx = minIdx = 0;
	maxVal = minVal = data[0];
	for(let i = 1; i < n; ++i) {
		const v = data[i];
		if(v < minVal) {
			minIdx = i; minVal = v;
		} else if(v > maxVal) {
			maxIdx = i; maxVal = v;
		}
	}
	this._minIdx = minIdx;
	this._minVal = minVal;
	this._maxIdx = maxIdx;
	this._maxVal = maxVal;
}

} );
