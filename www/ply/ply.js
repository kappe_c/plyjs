define(['ply/Loader', 'ply/SyntaxError',
        'ply/Element', 'ply/MissingElementError',
        'ply/ScalarProperty', 'ply/ArrayProperty', 'ply/MissingPropertyError',
        'ply/Saver'],
        (PlyLoader, PlySyntaxError,
         PlyElement, PlyMissingElementError,
         PlyScalarProperty, PlyArrayProperty, PlyMissingPropertyError,
         PlySaver) =>
/**
 * Object to export the public interface of the PLY library.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 13, 2017
 */
({

load: PlyLoader.load,
SyntaxError: PlySyntaxError,
Element: PlyElement,
MissingElementError: PlyMissingElementError,
ScalarProperty: PlyScalarProperty,
ArrayProperty: PlyArrayProperty,
MissingPropertyError: PlyMissingPropertyError,
save: PlySaver.save

}) );
