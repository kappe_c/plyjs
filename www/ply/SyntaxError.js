define(() => 
/**
 * Instances of this class are thrown when there is a syntax error in the PLY file being parsed.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date December 19, 2016
 */
class PlySyntaxError extends Error {

constructor(want, have)
{
	super();
	this.message = 'Expected "' + want + '", got "' + have + '"';
}

} );
