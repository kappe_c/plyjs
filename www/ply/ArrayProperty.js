define(['ply/Property'], (PlyProperty) => 
/**
 * Class representing a PLY property that stores arrays.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date December 19, 2016
 */
class PlyArrayProperty extends PlyProperty {

/** Verbose class name. */
static get label() { return 'PLY Array Property'; }

//------------------------------------------------------------------------------------------------//

/**
 * Construct a `PlyArrayProperty` from an array of arrays.
 * @param {string} name Name of the PLY property. 
 * @param {Array[]} data Array of arrays.
 * @param {string} [valueType='Float64'] Type of the values.
 * @param {string[]} [comments] Comments concerning the property.
 */
static fromData(name, data, valueType = 'Float64', comments)
{
	const obj = new PlyArrayProperty(undefined, valueType, name, 0, comments);
	obj._data = data;
	obj.compExtrema();
	const maxn = obj.maxVal;
	if(maxn < 256)
		obj._sizeType = 'Uint8';
	else if(maxn < Math.pow(2, 16))
		obj._sizeType = 'Uint16';
	else
		obj._sizeType = 'Uint32';
	return obj;
}

//------------------------------------------------------------------------------------------------//

/**
 * Constructor defined for the use in `PlyLoader.load`.
 * @param {number} [sizeType] - Basic data type to use when reading/writing the size of an array.
 * @param {number} [valueType] - Basic data type to use when reading/writing the values of an array.
 * One of the values in `PlyLoader.plyType2jsType`. 
 * @param {string} name - Name of this PLY property. 
 * @param {number} numItems - Number of items held by this PLY property, must be consistent with 
 * the number of objects in the parent PLY element.
 * @param {Array} [comments=[]] - Comments associated with this PLY property.
 */
constructor(sizeType, valueType, name, numItems, comments = [])
{
	super(name, comments);
	this._sizeType = sizeType;
	this._valueType = valueType;
	this._data = new Array(numItems);
}

/** Get the (JS) type of the sizes, such as "Uint8". */
get sizeType() { return this._sizeType; }

/** Get the (JS) type of the values, such as "Float64". */
get valueType() { return this._valueType; }

//------------------------------------------------------------------------------------------------//

/** Impl. of `PlyProperty` method. */
get hasFloats() { return this._valueType === 'Float64' || this._valueType === 'Float32'; }

//------------------------------------------------------------------------------------------------//

/** Impl. of `PlyProperty` method. */
compExtrema()
{
	const data = this._data;
	const n = data.length;
	let minIdx, minVal, maxIdx, maxVal;
	maxIdx = minIdx = 0;
	maxVal = minVal = data[0].length;
	for(let i = 1; i < n; ++i) {
		const v = data[i].length;
		if(v < minVal) {
			minIdx = i; minVal = v;
		} else if(v > maxVal) {
			maxIdx = i; maxVal = v;
		}
	}
	this._minIdx = minIdx;
	this._minVal = minVal;
	this._maxIdx = maxIdx;
	this._maxVal = maxVal;
}

} );
