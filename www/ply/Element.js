define(['ply/ScalarProperty', 'ply/ArrayProperty'], (PlyScalarProperty, PlyArrayProperty) => 
/**
 * Class representing a PLY element.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date December 19, 2016
 */
class PlyElement {

/** Verbose class name. */
static get label() { return 'PLY Element'; }

//------------------------------------------------------------------------------------------------//

/**
 * Constructor defined for the use in `PlyLoader.load`.
 * @param {string} name - Name.
 * @param {number} numObjects - Number of stored objects.
 * @param {string} [source='unnamed source'] - Source of the data, e.g. the name of a PLY file.
 * @param {Array} [comments=[]] - Associated lines of comments.
 * @param {Array} [properties=[]] - Associated PLY properties.
 */
constructor(name, numObjects, source = 'unnamed source', comments = [], properties = [])
{
	this._name = name;
	this._numObjects = numObjects;
	this._source = source;
	this._label = name + ' from ' + source;
	this._comments = comments;
	this._data = properties;
}

//------------------------------------------------------------------------------------------------//

/** "Class name" as defined in the header. */
get name() { return this._name; }

/** Array of PLY properties. */
get properties() { return this._data; }

/** The name of the source (PLY file) the data came from. */
get source() { return this._source; }

/** Label as it may be shown to the user (includes `name` and `source`). */
get label() { return this._label; }

/** An array of strings containing the "comment" lines directly above the element. */
get comments() { return this._comments; }

//------------------------------------------------------------------------------------------------//

/** Get the number of properties. */
get numProperties() { return this._data.length; }

/** Get the `p`th property. */
getProperty(p) { return this._data[p]; }

/** Get the (scalar- or array-valued) property by the given `name`. */
getAnyProp(name) { return this._data.find(prop => prop.name === name); }

/** Get the scalar-valued property by the given `name`. */
getScalarProp(name)
{
  return this._data.find(prop => prop.name === name && prop instanceof PlyScalarProperty);
}

/** Get the array-valued property by the given `name`. */
getArrayProp(name)
{
  return this._data.find(prop => prop.name === name && prop instanceof PlyArrayProperty);
}

//------------------------------------------------------------------------------------------------//

/** Get the number of objects. */
get numObjects() { return this._numObjects; }

/** Get the `o`-th object as a JS object with the PLY properties as properties. */
getObject(o)
{
	const res = {};
	this._data.forEach(prop => { res[prop.name] = prop.data[o]; });
	return res;
}

/**
 * Get the `o`-th object as a JS array with the PLY properties as values. 
 * A little more efficient but a little less pretty than the `getObject` method.
 */
getArray(o)
{
	return this._data.map(prop => prop.data[o]);
}

} );
