define(['ply/SyntaxError', 'ply/ScalarProperty', 'ply/ArrayProperty', 'ply/Element', 'ply/types'], 
			 (PlySyntaxError, PlyScalarProperty, PlyArrayProperty, PlyElement, types) => 
/**
 * Class containing the functionality to load PLY files. 
 * The most important function is the static `load`.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date December 19, 2016
 */
class PlyLoader {

/**
 * Print the header in JS-style syntax.
 * @private
 */
static printHeader(elems)
{
	let str = '';
	const ne = elems.length;
	for(let e = 0; e < ne; ++e)
	{
		const elem = elems[e];
		if(elem.comments.length !== 0)
			str += '// ' + elem.comments.join('\n// ') + '\n';
		str += elem.name + ' = { // ' + elem.numObjects + ' object(s) like this\n';
		for(let p = 0; p < elem.numProperties; ++p)
		{
			const prop = elem.getProperty(p);
			if(prop.comments.length !== 0)
				str += '\t// ' + prop.comments.join('\n\t// ') + '\n';
			str += '\t' + prop.name + ': ';
			if(prop instanceof PlyScalarProperty)
				str += prop.type + ',\n';
			else
				str += prop.valueType + 'Array(' + prop.sizeType + '),\n';
		}
		str += e < ne - 1 ? '}\n' : '}';
	}
	console.info(str);
}

//------------------------------------------------------------------------------------------------//

/**
 * Log the objects of a `PlyElement`.
 * @private
 */
static logElem(elem)
{
	const no = elem.numObjects;
	const np = elem.numProperties;
	console.log(elem.name + ' objects:');
	for(let o = 0; o < no; ++o)
	{
// 		let line = '';
// 		for(let p = 0; p < np; ++p)
// 		{
// 			if(elem[p] instanceof Array)
// 				line += '[' + elem[p][o].join() + '] ';
// 			else
// 				line += elem[p][o] + ' ';
// 		}
// 		console.log(line);
		console.log(elem.getObject(o));
// 		console.log(elem.getArray(o));
	}
}

//------------------------------------------------------------------------------------------------//

/**
 * Parse the header of the file and read the data.
 * @param {ArrayBuffer} arrayBuffer The binary string which was the content of a PLY file.
 * @param {string} source Name of the source file or similar; used to set the `source` property 
 * of the resulting `PLYElement`s.
 * @param {boolean} [wantVerbose=true] Print the parsed header via `console.info`?
 * @return Array of `PlyElement`s. Contains the property `source` which holds the respective 
 * argument, and also the method `getElement` to get an element (or undefined) by name; further 
 * arguments to this function may be the names of required properties, where array-valued 
 * properties must be flagged by a subsequent `true` argument; the order of properties is 
 * unimportant; e.g. `elems.getElement('face', 'green', 'vertex_index', true, 'red', 'green')`.
 * @throw {PlySyntaxError}
 */
static load(arrayBuffer, source, wantVerbose = false)
{
	const dv = new DataView(arrayBuffer);
	let i = -1; // offset pointing before the next unread byte
	const cc2str = String.fromCharCode;
	
	const consumeWord = () => { // limited by space (32)
		let res = '';
		for(let c = dv.getUint8(++i); c !== 32; c = dv.getUint8(++i))
			res += cc2str(c);
		return res;
	}
	
	const consumeLine = () => { // limited by line feed (10)
		let res = '';
		for(let c = dv.getUint8(++i); c !== 10; c = dv.getUint8(++i))
			res += cc2str(c);
		return res;
	}
	
	
	// The first two lines are fixed.
	
	const plyKey = consumeLine();
	if(plyKey !== 'ply')
		throw new PlySyntaxError('ply', plyKey);
	
	const formatKey = consumeWord();
	if(formatKey !== 'format')
		throw new PlySyntaxError('format', formatKey);
	
	const format = consumeWord();
	if(format !== 'ascii' && format != 'binary_little_endian' && format != 'binary_big_endian' )
		throw new PlySyntaxError('ascii|binary_little_endian|binary_big_endian', format);
	
	const versionKey = consumeLine();
	if(versionKey !== '1.0')
		throw new PlySyntaxError('1.0', versionKey);
	
	
	// Parse the rest of the header, i.e. "comment", "element" and "property" lines.
	// Collect the information per element to define reader functions later
	// (which also depend on the `format`).
	const elems = [];
	let comments = [];
	
	while(true) // break on "end_header"
	{
		let lineKey = '';
		for(let c = dv.getUint8(++i); c !== 10 && c !== 32; c = dv.getUint8(++i))
			lineKey += cc2str(c);
		
		// Handle cases sorted by likelihood.
		if(lineKey === 'property')
		{
			if(elems.length === 0)
				throw new PlySyntaxError('element|comment', 'property');
			
			const elem = elems[elems.length - 1];
			let type, sizeType, valueType;
			
			let tmpType = consumeWord();
			type = types.ply2js[tmpType];
			if(type === undefined)
				throw new PlySyntaxError('char|uchar|short|ushort|int|uint|float|double|list', tmpType);
			
			if(type === 'Array')
			{
				tmpType = consumeWord();
				sizeType = types.ply2js[tmpType];
				if(sizeType === undefined || sizeType === 'Array')
					throw new PlySyntaxError('char|uchar|short|ushort|int|uint|float|double', tmpType);
				
				tmpType = consumeWord();
				valueType = types.ply2js[tmpType];
				if(valueType === undefined || valueType === 'Array')
					throw new PlySyntaxError('char|uchar|short|ushort|int|uint|float|double', tmpType);
			}
			
			const name = consumeLine();
			elem._data.push(type !== 'Array' ? 
				new PlyScalarProperty(type, name, elem.numObjects, comments) : 
				new PlyArrayProperty(sizeType, valueType, name, elem.numObjects, comments));
			comments = [];
		}
		else if(lineKey === 'element')
		{
			const elemName = consumeWord();
			const noStr = consumeLine();
			const no = parseInt(noStr, 10);
			if(isNaN(no) || no < 1)
				throw new PlySyntaxError('integer > 0', noStr);
			elems.push(new PlyElement(elemName, no, source, comments));
			comments = [];
		}
		else if(lineKey === 'comment')
			comments.push(consumeLine());
		else if(lineKey === 'end_header')
			break;
	}
	const ne = elems.length;
	if(wantVerbose)
		PlyLoader.printHeader(elems);
	
	
	// Define the reading functions.
	
	const e2p2read = new Array(ne);
	if(format === 'ascii')
	{
		for(let e = 0; e < ne; ++e)
		{
			const elem = elems[e];
			const np = elem.numProperties;
			const p2read = new Array(np); e2p2read[e] = p2read;
			const pMax = np - 1; // last property ends on '\n'
			for(let p = 0; p <= pMax; ++p)
			{
				const prop = elem.getProperty(p);
				const itms = prop.data;
				if(prop instanceof PlyScalarProperty)
				{
					if(p !== pMax)
						p2read[p] = prop.hasFloats ? 
							o => { itms[o] = parseFloat(consumeWord()); } : 
							o => { itms[o] = parseInt(  consumeWord()); };
					else
						p2read[p] = prop.hasFloats ? 
							o => { itms[o] = parseFloat(consumeLine()); } : 
							o => { itms[o] = parseInt(  consumeLine()); };
				}
				else
				{
					const st = prop.sizeType;
					const vt = prop.valueType;
					const consumeSize = (st === 'Float32' || st === 'Float64') ?
						() => parseFloat(consumeWord()) : () => parseInt(consumeWord());
					const consumeValue = prop.hasFloats ?
						() => parseFloat(consumeWord()) : () => parseInt(consumeWord());
					const consumeLastValue = prop.hasFloats ?
						() => parseFloat(consumeLine()) : () => parseInt(consumeLine());
					if(p !== pMax)
						p2read[p] = o => {
							const n = consumeSize();
							const arr = itms[o] = new Array(n);
							for(let j = 0; j < n; ++j)
								arr[j] = consumeValue();
						};
					else
						p2read[p] = o => {
							const n = consumeSize();
							const arr = itms[o] = new Array(n);
							for(let j = 0; j < n - 1; ++j)
								arr[j] = consumeValue();
							arr[n-1] = consumeLastValue();
						};
				}
			} // END property
		} // END element
	} // END ascii
	else
	{
		const isLittleEndian = format === 'binary_little_endian';
		++i; // Now `i` shall point directly to the next byte.
		for(let e = 0; e < ne; ++e)
		{
			const elem = elems[e];
			const np = elem.numProperties;
			const p2read = e2p2read[e] = new Array(np);
			for(let p = 0; p < np; ++p)
			{
				const prop = elem.getProperty(p);
				const itms = prop.data;
				if(prop instanceof PlyScalarProperty)
				{
					const tSize = types.js2size[prop.type];
					const m = 'get' + prop.type;
					p2read[p] = tSize === 1 ? 
						o => { itms[o] = dv[m](i++); } : 
						o => { itms[o] = dv[m](i, isLittleEndian); i += tSize; };
				}
				else
				{
					const st = prop.sizeType;
					const stSize = types.js2size[st];
					const stm = 'get' + st;
					const vt = prop.valueType;
					const vtSize = types.js2size[vt];
					const vtm = 'get' + vt;
					// NOTE There are optimization possibilities (like above) for 1-byte types.
					p2read[p] = o => {
						const n = dv[stm](i, isLittleEndian);
						i += stSize;
						const arr = itms[o] = new Array(n);
						for(let j = 0; j < n; ++j) {
							arr[j] = dv[vtm](i, isLittleEndian);
							i += vtSize;
						}
					};
				}
			} // END property
		} // END element
	} // END binary
	
	
	for(let e = 0; e < ne; ++e) {
		const no = elems[e].numObjects;
		const p2read = e2p2read[e];
		for(let o = 0; o < no; ++o)
			for(let read of p2read)
				read(o);
	}
	
	
	// Compute the extreme values. Also print the objects if there are less than four.
	for(let elem of elems) {
		if(elem.numObjects <= 4 && wantVerbose)
			PlyLoader.logElem(elem);
		const np = elem.numProperties;
		for(let p = 0; p < np; ++p)
			elem.getProperty(p).compExtrema();
	}
	
  elems.source = source;
  elems.getElement = function(eName)
  {
    const res = this.find(elem => elem.name === eName);
    if(res === undefined)
      return undefined;
    
    for(let i = 1; i < arguments.length; ++i)
    {
      if(arguments[i + 1] !== true) {
        if(res.getScalarProp(arguments[i]) === undefined)
          return undefined;
      } else {
        if(res.getArrayProp(arguments[i++]) === undefined) // i++ to jump over flag
          return undefined;
      }
    }
    return res;
  };
  return elems;
}

} );
