define(() => 
/**
 * Module providing maps from PLY to JS types and vice versa. Also the respective sizes in byte.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date December 19, 2016
 */
({ // parantheses around the braces because they shall begin an object, not a block
	
	/** PLY type to JS type. */
	ply2js:
	{
		char:    'Int8',   uchar:  'Uint8',
		short:   'Int16',  ushort: 'Uint16',
		int:     'Int32',  uint:   'Uint32',
		float: 'Float32', double: 'Float64', list: 'Array'
	},
	
	/** JS type to PLY type. */
	js2ply:
	{
		  Int8:  'char',   Uint8:  'uchar',
		  Int16: 'short',  Uint16: 'ushort',
		  Int32: 'int',    Uint32: 'uint',
		Float32: 'float', Float64: 'double', Array: 'list'
	},
	
	/** PLY type to number of bytes. */
	ply2size:
	{
		char:  1, uchar:  1,
		short: 2, ushort: 2,
		int:   4, uint:   4,
		float: 4, double: 8
	},
	
	/** JS type to number of bytes. */
	js2size:
	{
			Int8:  1,  Uint8:  1,
			Int16: 2,  Uint16: 2,
			Int32: 4,  Uint32: 4,
		Float32: 4, Float64: 8
	}
	
}) );
