define(() => 
/**
 * Abstract base class for PLY properties.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 11, 2017
 */
class PlyProperty {

/**
 * @param {string} name - Name of this PLY property. 
 * @param {Array} [comments=[]] - Comments associated with this PLY property.
 */
constructor(name, comments = [])
{
	this.name = name;
	this.comments = comments;
}

/** Get the items stored by this property. */
get data() { return this._data; }

//------------------------------------------------------------------------------------------------//

/**
 * Check if the values are a floating point type. Purely virtual.
 * @abstract
 */
get hasFloats() {}

//------------------------------------------------------------------------------------------------//

/**
 * Compute the extreme values and the indices at which they occur. Purely virtual.
 * @abstract
 */
compExtrema() {}

/** The data index where the minimum value occurs. */
get minIdx() { return this._minIdx; }

/** The minimum scalar value or array length. */
get minVal() { return this._minVal; }

/** The data index where the maximum value occurs. */
get maxIdx() { return this._maxIdx; }

/** The maximum scalar value or array length. */
get maxVal() { return this._maxVal; }

} );
