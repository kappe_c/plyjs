define(['ply/ScalarProperty', 'ply/ArrayProperty', 'ply/Element', 'ply/types'], 
			 (PlyScalarProperty, PlyArrayProperty, PlyElement, types) => 
/**
 * Class containing the functionality to save PLY files. 
 * The most important function is the static `load`.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date December 19, 2016
 */
class PlySaver {

/**
 * Write the PLY header and data to a Blob which is returned.
 * @param {Array} elems - An array of `PlyElement`s.
 * @param {string} [format='binary_little_endian'] - The format to save the PLY data. 
 * Either 'ascii' or 'binary_big_endian' or 'binary_little_endian'.
 */
static save(elems, format)
{
	if(format !== 'ascii' && format !== 'binary_big_endian' && format !== 'binary_little_endian')
		format = 'binary_little_endian';
	let dataSize = 0;
	const ne = elems.length;
	
	
	// Compose the header and compute the size of the binary data on disk.
	
	let hdr = 'ply\nformat ' + format + ' 1.0\n';
	for(let e = 0; e < ne; ++e)
	{
		const elem = elems[e];
		for(let com of elem.comments)
			hdr += 'comment ' + com + '\n';
		hdr += 'element ' + elem.name + ' ' + elem.numObjects + '\n';
		const no = elem.numObjects;
		for(let p = 0; p < elem.numProperties; ++p)
		{
			const prop = elem.getProperty(p);
			const itms = prop.data;
			for(let com of prop.comments)
				hdr += 'comment ' + com + '\n';
			let typeStr;
			if(prop instanceof PlyScalarProperty)
			{
				typeStr = types.js2ply[prop.type];
				dataSize += no * types.ply2size[typeStr];
			}
			else
			{
				typeStr = 'list ' + types.js2ply[prop.sizeType] + ' ' + types.js2ply[prop.valueType];
				const nv = itms.reduce((acc, cur) => acc + cur.length, itms[0].length);
				dataSize += no * types.js2size[prop.sizeType] + nv * types.js2size[prop.valueType];
			}
			hdr += 'property ' + typeStr + ' ' + prop.name + '\n';
		}
	}
	hdr += 'end_header\n';
	
	
	const e2p2write = new Array(ne);
	// Write the data to a string for the ascii format.
	
	if(format === 'ascii')
	{
		let str = '';
		for(let e = 0; e < ne; ++e)
		{
			const elem = elems[e];
			const np = elem.numProperties;
			const p2write = new Array(np); e2p2write[e] = p2write;
			// Define write functions.
			const pMax = np - 1;
			for(let p = 0; p < pMax; ++p) {
				const prop = elem.getProperty(p);
				const itms = prop.data;
				p2write[p] = prop instanceof PlyScalarProperty ?
					o => { str += itms[o] + ' '; } : 
					o => {
						const arr = itms[o];
						str += arr.length + ' ' + arr.join(' ') + ' ';
					};
			}
			const prop = elem.getProperty(pMax);
			const itms = prop.data;
			p2write[pMax] = prop instanceof PlyScalarProperty ?
				o => { str += itms[o] + '\n'; } : 
				o => {
					const arr = itms[o];
					str += arr.length + ' ' + arr.join(' ') + '\n';
				};
			// Call write functions.
			const no = elem.numObjects;
			for(let o = 0; o < no; ++o)
				for(let write of p2write)
					write(o);
		}
		
		return new Blob([hdr, str], { type: 'application/ply;charset=us-ascii' });
	}
	
	
	// Write the data to a buffer for the binary format.
	
	const hdrBinary = new Uint8Array(hdr.length);
	for(let i = 0; i < hdr.length; ++i)
		hdrBinary[i] = hdr.charCodeAt(i);
	
	const data = new ArrayBuffer(dataSize);
	const dv = new DataView(data);
	let i = 0;
	const wantLittleEndian = format === 'binary_little_endian';
	
	for(let e = 0; e < ne; ++e)
	{
		const elem = elems[e];
		const np = elem.numProperties;
		const p2write = new Array(np); e2p2write[e] = p2write;
		// Define write functions.
		for(let p = 0; p < np; ++p)
		{
			const prop = elem.getProperty(p);
			const itms = prop.data;
			if(prop instanceof PlyScalarProperty)
			{
				const m = 'set' + prop.type;
				const vs = types.js2size[prop.type];
				p2write[p] = vs === 1 ? 
					o => { dv[m](i++, itms[o]); } : 
					o => {
						dv[m](i, itms[o], wantLittleEndian);
						i += vs;
					};
			}
			else
			{
				const ss = types.js2size[prop.sizeType];
				const sm = 'set' + prop.sizeType;
				const vs = types.js2size[prop.valueType];
				const vm = 'set' + prop.valueType;
				p2write[p] = o => {
					const arr = itms[o];
					dv[sm](i, arr.length, wantLittleEndian);
					i += ss;
					for(let v of arr) {
						dv[vm](i, v, wantLittleEndian);
						i += vs;
					}
				};
			}
		}
		// Call write functions.
		const no = elem.numObjects;
		for(let o = 0; o < no; ++o)
			for(let write of p2write)
				write(o);
	}
	
	return new Blob([hdrBinary, data], { type: 'application/ply;charset=us-ascii' });
}

} );
