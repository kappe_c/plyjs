define(() => 
/**
 * This class is not actually used by PlyJS itself but provided for users.
 * Instances of this class could be thrown after a call like `vertexElem.get*Property('propName')`
 * (where * is either "Any", "Scalar" or "Array") when a software requires a certain element to
 * have a certain property (of a certain kind) be present in a PLY file.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date June 5, 2018
 */
class PlyMissingPropertyError extends Error {

/**
 * @param {string} pName Name of the required PLY property.
 * @param {PlyElement} [elem] PLY element that should have the property.
 * @param [kind] True for array, false for scalar, undefined for any.
 */
constructor(pName, elem, kind)
{
  super();
  switch(kind)
  {
    case false: kind = 'scalar '; break;
    case true: kind = 'list '; break;
    default: kind = '';
  }
  this.message = 'A "' + pName + '" ' + kind + 'property is required';
  if(elem !== undefined)
    this.message += ' for element "' + elem.name + '" in "' + elem.source + '"';
}

} );
