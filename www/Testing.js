define(['ply/Loader', 'ply/ScalarProperty', 'ply/Saver'], 
			 (PlyLoader, PlyScalarProperty, PlySaver) => 
/**
 * Class offering several auxiliary functions to test the PLY library.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 12, 2017
 */
class Testing {

/**
 * Asynchronously read the content of the given `file` into an `ArrayBuffer` which is then 
 * passed to the callback function `cb` as its first argument; the second argument is the file name.
 */
static read(file, cb)
{
	const reader = new FileReader();
	reader.onload = () => { cb(reader.result, file.name); };
	reader.readAsArrayBuffer(file);
}

//------------------------------------------------------------------------------------------------//

/**
 * Offer to download the given `blob` as a file called `fileName`.
 */
static saveAs(blob, fileName)
{
	const a = document.createElement('a');
	a.style.display = 'none';
	a.href = URL.createObjectURL(blob);
	a.download = fileName;
	a.target = '_blank';
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
}

//------------------------------------------------------------------------------------------------//

/**
 * Create a *table* DOM element from a PLY element.
 */
static tableize(elem)
{
	const tbl = document.createElement('table');
	tbl.className = 'dataTbl';
	
	const headersRow = document.createElement('tr');
	const th = document.createElement('th');
	th.appendChild(document.createTextNode('')); // 'index'
	headersRow.appendChild(th);
	for(let prop of elem.properties) {
		const th = document.createElement('th');
		th.appendChild(document.createTextNode(prop.name));
		headersRow.appendChild(th);
	}
	tbl.appendChild(headersRow);
	
	const no = elem.numObjects;
	const np = elem.numProperties;
	for(let o = 0; o < no; ++o) {
		const tr = document.createElement('tr');
		const td = document.createElement('td');
		td.appendChild(document.createTextNode(o));
		tr.appendChild(td);
		for(let p = 0; p < np; ++p) {
			const prop = elem.getProperty(p);
			const data = prop.data;
			const td = document.createElement('td');
			let str;
			if(prop instanceof PlyScalarProperty)
				str = !prop.hasFloats ? data[o] : data[o].toPrecision(4);
			else {
				if(!prop.hasFloats)
					str = data[o].join();
				else {
					const inp = data[o];
					const n = inp.length;
					const out = new Array(n);
					for(let i = 0; i < n; ++i)
						out[i] = inp[i].toPrecision(4);
					str = out.join();
				}
			}
			td.appendChild(document.createTextNode(str));
			tr.appendChild(td);
		}
		tbl.appendChild(tr);
	}
	
	return tbl;
}

//------------------------------------------------------------------------------------------------//

/**
 * Load the PLY file, render each of the contained elements in a table, and offer to save a copy. 
 * Intended as callback function for the `read` function.
 */
static test(arrBuf, source, outFormat)
{
	let elems;
	try {
		elems = PlyLoader.load(arrBuf, source);
	} catch(e) {
		console.error(e.message);
		alert(e.message);
		return;
	}
	
	const div = document.getElementById('outputDiv');
	while(div.lastChild)
		div.removeChild(div.lastChild);
	
	for(let elem of elems)
	{
		const h2 = document.createElement('h2');
		h2.appendChild(document.createTextNode(elem.name));
		div.appendChild(h2);
		div.appendChild(Testing.tableize(elem));
	}
	
	if(outFormat === false)
		return;
	const outFileName = source.replace('.ply', '.copy.ply');
	Testing.saveAs(PlySaver.save(elems, outFormat), outFileName);
}

} );
