{
	baseUrl: '../www',
// 	dir: '../www-built',
// 	skipDirOptimize: true
	name: 'ply/ply',
// 	include: [],
	out: '../ply.js',
// 	modules: [{ 'name': 'ply/ply' }],
	optimize: 'none',
// 	optimize: 'closure',
// 	closure: { CompilerOptions: {
// 		languageIn:  Packages.com.google.javascript.jscomp.CompilerOptions.LanguageMode.valueOf('ECMASCRIPT6_STRICT'),
// 		languageOut: Packages.com.google.javascript.jscomp.CompilerOptions.LanguageMode.valueOf('ECMASCRIPT5')
// 	} }
	// We want an anonymous module so that the path of the "ply.js" file can be used in require 
	// calls of web apps that use this library.
	onBuildWrite: (moduleName, path, contents) => moduleName !== 'ply/ply' ?
		contents : contents.replace("define('ply/ply',[", "define([")
}
