# Usage

The library should be loaded using requirejs. Then `ply.load` can be used to load the data from a 
PLY file that is given as an `ArrayBuffer` object.
The data from the file is stored in a property-centric rather than an object-oriented manner; 
so one gets a structure of arrays, not an array of structures.

```javascript
/**
 * Just log all the vertices and faces that have been loaded.
 * @param elems Array of PLY elements.
 */
const onPlyElems = elems =>
{
	const vElem = elems.getElement('vertex');
	const xProp = vElem.getScalarProp('x');
	const yProp = vElem.getScalarProp('y');
	const v2x = xProp.data;
	const v2y = yProp.data;
	
	const nv = vElem.numObjects;
	for(let v = 0; v < nv; ++v)
		console.log(`(${v2x[v]}, ${v2y[v]})`);
	
	const fElem = elems.getElement('face');
	const vsProp = fElem.getArrayProp('vertex_index');
	const f2vs = vsProp.data;
	
	const nf = fElem.numObjects;
	for(let f = 0; f < nf; ++f) {
		const vs = f2vs[f];
		console.log(`f${f} (${vs.length} vertices): ${vs.join(', ')}`);
	}
};

// Auxiliary function not really related to PlyJS.
/**
 * Append a file input element to the document body. 
 * @param cb Callback function that gets called with an array of PLY elements.
 * @return The input element.
 */
const createOpenButton = (cb) =>
{
	const fileInp = document.createElement('input');
	fileInp.type = 'file';
	fileInp.onchange = () => {
		const file = fileInp.files[0];
		const reader = new FileReader();
		reader.onload = () => {
			let elems;
			try { elems = ply.load(reader.result, file.name); }
			catch(e) {
				if(e instanceof ply.SyntaxError)
					alert(e.message);
				else throw e;
			}
			cb(elems);
		};
		reader.readAsArrayBuffer(file);
	};
	document.body.appendChild(fileInp);
	return fileInp;
};

const openBtn = createOpenButton(onPlyElems);
// openBtn.click();

```


# Test App

A simple web app is also part of this repository. It can show the contents of a PLY file in 
tables and convert between ascii, little endian and big endian format.


# Technical Information

Published under the LGPL license, this web project contains the following files and directories:

- tools/ - Build tools to optimize the project.
- www/ - Web assets for the project.
    - ply/ - Directory to store the PLY library scripts.
    - index.html - Entry point into the test app.
    - main.js - Top-level script used by index.html.
    - require.js - JS library for dependency management that is used here.
    - table.css - Style for the created tables.
    - Testing.js - Test driver for the PLY library.
- license.txt - Text of the LGPL which governs this project.
- package.json - Package configuration.
- readme.md - This readme file.

To build the library, run:
```
node tools/r.js -o tools/build.js
```

This build command creates an optimized version of the project in the file ply.js 
(in the same directory as this readme file) which will be optimized to include all of its 
dependencies.

For more information on the optimizer:
[http://requirejs.org/docs/optimization.html](http://requirejs.org/docs/optimization.html)

For more information on using requirejs:
[http://requirejs.org/docs/api.html](http://requirejs.org/docs/api.html)

The latest stable version of the built library is also directly available under *Downloads*.
